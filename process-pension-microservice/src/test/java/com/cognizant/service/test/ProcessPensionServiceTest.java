package com.cognizant.service.test;

import static org.junit.Assert.assertNotNull;

//import static org.junitassertNotNull;

//import java.text.ParseException;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.cognizant.model.PensionDetail;
import com.cognizant.model.PensionerDetail;
import com.cognizant.model.PensionerInput;
import com.cognizant.service.ProcessPensionService;

@SpringBootTest
public class ProcessPensionServiceTest {
	@Test
	public void ProcessPensionServiceAllArgsTest() {
		ProcessPensionService processPensionService = new ProcessPensionService();
		PensionerDetail pensionerDetail = new PensionerDetail();
		PensionerInput pensionerInput = new PensionerInput();
		PensionDetail pensionDetail = processPensionService.getPensionDetail(pensionerDetail, pensionerInput);
		assertNotNull(pensionDetail);
		PensionDetail pensionDetail1 = processPensionService.savePensionDetail(pensionDetail);
		assertNotNull(pensionDetail1);
	}

}
