package com.cognizant.model.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.meanbean.test.BeanTester;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.cognizant.exception.ControllerAdviceClass;
import com.cognizant.exception.ErrorResponse;
import com.cognizant.exception.ResourceNotFound;
import com.cognizant.model.AuthRequest;
import com.cognizant.model.User;
import com.cognizant.util.JwtUtil;

@SpringBootTest
@RunWith(SpringRunner.class)
public class AuthorizationModelTest {

	@Test
	public void testPensionerBean() {
		final BeanTester beanTester = new BeanTester();
		beanTester.getFactoryCollection();
		beanTester.testBean(User.class);
	}

	@Test
	public void AuthRequestAllArgTest() {
		AuthRequest authrequest = new AuthRequest("Shiva", "1234");
		assertNotNull(authrequest);
	}

	@Test
	public void AuthRequestNoArgTest() {
		AuthRequest authrequest = new AuthRequest();
		authrequest.setPassword("1234");
		authrequest.setUserName("Shiva");
		assertEquals("1234", authrequest.getPassword());
		assertEquals("Shiva", authrequest.getUserName());

		assertNotNull(authrequest);
	}

	@Test
	public void ErrorExceptionAllArgsTest() {
		ErrorResponse errorResponse = new ErrorResponse("Shiva", 550, 2L);
		assertNotNull(errorResponse);
	}

	@Test
	public void ErrorExceptionNoArgsTest() {
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setExceptionTime(2L);
		errorResponse.setMessage("Shiva");
		errorResponse.setStatusCode(550);
		assertEquals(String.valueOf(2L), String.valueOf(errorResponse.getExceptionTime()));
		assertEquals("Shiva", errorResponse.getMessage());
		assertEquals(550, errorResponse.getStatusCode());
		assertNotNull(errorResponse);
	}

	@Test
	public void ControllerAdviceClassTest() {
		ControllerAdviceClass advice = new ControllerAdviceClass();
		ResourceNotFound resource = new ResourceNotFound();
		ResponseEntity<ErrorResponse> response = advice.controllerAdviceResponse(resource);
		assertNotNull(response);
	}

	@Test
	public void ResourceNotFoundNoArgsTest() {
		ResourceNotFound resource = new ResourceNotFound();
		resource.setMessage("Shiva");
		assertEquals("Shiva", resource.getMessage());
		assertNotNull(ResourceNotFound.getSerialversionuid());
	}

	@Test
	public void ResourceNotFoundAllArgsTest() {
		ResourceNotFound resource = new ResourceNotFound("Shiva");
		assertNotNull(resource);
	}

	/*
	 * @Test public void JwtUtilTest() { JwtUtil jwtUtil = new JwtUtil(); String s =
	 * jwtUtil.extractUsername("User2","password2"); assertNotNull(s); }
	 */
}
